# Hash Hax

Objective of this project is to provide a Tampermonkey script that hacks the content and name of uploaded media to the chans. The idea is to prevent a variety of tracking efforts based on filenames and media contents. By editing files in memory prior to upload, these hacks should defeat hash, name, and conceptual matching.
